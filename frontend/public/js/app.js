const apiUrl = window.location.protocol + "//" + window.location.hostname + ":8081";
const expButtons = document.querySelectorAll('.expr');

/**
 * Object to store pressed history for "clear" buttons functionality
 */
const history = {

    _curSymbol: "",

    _symbolHist: [],

    addKeyPressed: function (value) {
        if (isNaN(value) && value !== ".") {
            this._addNotNumericValue(value);
            return;
        }
        this._addNumericValue(value);
    },

    _addNumericValue: function (value) {

        let prevSymbol = (this._symbolHist.length) ? this._symbolHist[this._symbolHist.length - 1] : "";

        if (!this._curSymbol.length) {
            this._curSymbol = value;
            this._symbolHist.push(this._curSymbol);
            return;
        }

        if (isNaN(this._curSymbol) && this._curSymbol !== ".") {
            this._symbolHist.push(value);
            this._curSymbol = value;
            return;
        }

        if (!isNaN(prevSymbol) || prevSymbol === ".") {
            prevSymbol = this._symbolHist.pop();
        }
        this._curSymbol = prevSymbol.concat(value);
        this._symbolHist.push(this._curSymbol);

    },

    _addNotNumericValue: function (value) {
        if (!this._curSymbol.length) {
            this._curSymbol = value;
            this._symbolHist.push(this._curSymbol);
            return;
        }

        this._curSymbol = value;
        this._symbolHist.push(value);
    },

    clear: function () {
        this._curSymbol = "";
        this._symbolHist = [];
    },

    clearLast: function () {
        if (!this._symbolHist.length) {
            return;
        }
        this._symbolHist.pop();
        this._curSymbol = (this._symbolHist.length) ? this._symbolHist[this._symbolHist.length - 1] : "";
    },

    clearLastPressed: function () {
        if (!this._symbolHist.length) {
            return;
        }
        this._curSymbol = this._symbolHist.pop();
        if (isNaN(this._curSymbol)) {
            this._curSymbol = (this._symbolHist.length) ? this._symbolHist[this._symbolHist.length - 1] : "";
            return;
        }
        if (this._curSymbol.length < 2) {
            this._curSymbol = (this._symbolHist.length) ? this._symbolHist[this._symbolHist.length - 1] : "";
            return;
        }

        this._curSymbol = this._curSymbol.slice(0, -1);
        this._symbolHist.push(this._curSymbol);
    },

    changeLast: function (symbolTo) {
        if (!this._symbolHist.length) {
            return;
        }
        this.setLast(symbolTo);
    },

    setLast: function(value) {
        this._symbolHist.pop();
        this._curSymbol = value;
        this._symbolHist.push(this._curSymbol);
    },

    getCurrent: function () {
        return this._curSymbol;
    },

    toString: function () {
        let str = this._symbolHist.join("");
        if (str === "") {
            str = "0";
        }
        return str;
    }
};

const calcApp = {

    _errorHolder: document.querySelector('#error'),
    _result: document.querySelector('#result'),

    validator: {
        constList: ['pi', 'e'],

        pointValidate: function() {
            const symbol = history.getCurrent();
            if (!symbol) {
                return true;
            }

            if (symbol.includes('.')) {
                return false;
            }

            if (this.constList.includes(symbol)) {
                return false;
            }

            return true;
        },

        openParenthesysValidate: function() {
            const symbol = history.getCurrent();
            if (!symbol || symbol === "(" || this.isOperator(symbol)) {
                return true;
            }

            return false;
        },

        closeParenthesysValidate: function() {
            const symbol = history.getCurrent();
            if (!symbol || this.isOperator(symbol)) {
                return false;
            }
            const expr = history.toString();
            const openCount = (expr.match(/\(/g)||[]).length;
            const closeCount = (expr.match(/\)/g)||[]).length;
            if (openCount <= closeCount) {
                return false;
            }
            return true;
        },

        isOperator: function(value) {
            return (["+", "-", "*", "/"].includes(value));
        }
    },

    expBtnClick: function (value) {
        this.clearError();
        if (!this.validate(value)) {
            return;
        }

        history.addKeyPressed(value);
        this.setResult(history.toString());
    },

    clearBtnClick: function () {
        this.clearError();
        history.clear();
        this.setResult(history.toString());
    },

    clearLastBtnClick: function () {
        history.clearLast();
        this.setResult(history.toString());
    },

    backSpaceBtnClick: function () {
        history.clearLastPressed();
        this.setResult(history.toString());
    },

    negateBtnClick: function () {
        this.clearError();
        let symbol = history.getCurrent();
        if (!symbol || isNaN(symbol)) {
            return;
        }
        history.changeLast(symbol * (-1));
        this.setResult(history.toString());
    },

    validate: function(value) {
        if (value === ".") {
            return this.validator.pointValidate();
        }

        if (value === "(") {
            return this.validator.openParenthesysValidate();
        }

        if (value === ")") {
            return this.validator.closeParenthesysValidate();
        }

        return true;
    },

    calc: function () {
        const _self = this;
        let calcEl = document.querySelector('#calc-wrapper');
        this.beforeCalc(calcEl);

        let xhr = new XMLHttpRequest();
        xhr.open("POST", apiUrl, true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

        xhr.onreadystatechange = function () {
            if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
                let result = xhr.responseText;
                if (result.includes("Error:")) {
                    _self.setError(result);
                    result = 0;
                }
                history.clear();
                history.setLast(result);
                _self.setResult(history.toString());

            }
            _self.afterCalc(calcEl);
        };
        xhr.onerror = function () {
            alert("Error: Unable to fetch result from api");
        };
        xhr.send("expr=" + encodeURIComponent(txtResult.value));
    },

    beforeCalc: function(calcEl) {
        this.clearError();
        calcEl.classList.add("disable");
        calcEl.querySelector('#btnEq').value = "";
    },

    afterCalc: function (calcEl) {
        calcEl.classList.remove("disable");
        calcEl.querySelector('#btnEq').value = " = ";
    },

    clearError: function() {
      this.setError("");
    },

    setError: function (errorMsg) {
        this._errorHolder.value = errorMsg;
    },

    setResult: function (result) {
        this._result.value = result;
    }

};

for (let i = 0; i < expButtons.length; i++) {
    expButtons[i].addEventListener("click", function (e) {
        e.preventDefault();
        calcApp.expBtnClick(this.dataset.value);
    });
}

document.querySelector('#btnClear').addEventListener("click", function (e) {
    e.stopPropagation();
    e.preventDefault();

    calcApp.clearBtnClick();
});

document.querySelector('#btnClearLast').addEventListener("click", function (e) {
    e.stopPropagation();
    e.preventDefault();

    calcApp.clearLastBtnClick();
});

document.querySelector('#btnBackSpace').addEventListener("click", function (e) {
    e.stopPropagation();
    e.preventDefault();
    calcApp.backSpaceBtnClick();
});

document.querySelector('#btnEq').addEventListener("click", function (e) {
    e.stopPropagation();
    e.preventDefault();

    calcApp.calc();
});

document.querySelector('#btnNegate').addEventListener("click", function (e) {
    e.stopPropagation();
    e.preventDefault();
    calcApp.negateBtnClick();
});

document.addEventListener("keypress", function(e){
    const charCode = e.which || e.keyCode;
    const char = String.fromCharCode(charCode);
    const possibleKeys = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ".", "+", "-", "*", "/", "(", ")"];

    if (possibleKeys.includes(char)) {
        calcApp.expBtnClick(char);
        return;
    }

    if (charCode === 8) {
        calcApp.backSpaceBtnClick();
    }

    if (charCode === 13 || char === "=") {
        calcApp.calc();
    }
});

document.addEventListener("keydown", function(e){
    if(e.keyCode === 8) {
        e.preventDefault();
        calcApp.backSpaceBtnClick();
    }
});