<?php
require_once __DIR__ . '/../vendor/autoload.php';

if (empty($_POST) || empty($_POST['expr'])) {
    die('0');
}

$line = filter_var($_POST['expr'], FILTER_SANITIZE_STRING);

$calc = new \Calculator\Calculator();
echo $calc->calc($line);
