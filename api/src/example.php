<?php
/**
 */
require_once __DIR__ . '/../vendor/autoload.php';

$calc = new \Calculator\Calculator();
$lines = [
    'var=5+6',
    '5+var',
    '3/0',
];

foreach ($lines as $line) {
    echo $calc->calc($line);
    echo "\n";
}

