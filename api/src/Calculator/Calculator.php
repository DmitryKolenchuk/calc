<?php
/**
 */

namespace Calculator;

use Calculator\Exceptions\CalculateException;
use Calculator\Exceptions\ValidateException;
use Calculator\Nodes\AbstractNode;
use Calculator\Nodes\GroupNode;
use Calculator\Nodes\SymbolNode;
use Calculator\Symbols\AbstractOperation;
use Calculator\Symbols\AbstractSymbol;
use Calculator\Symbols\CloseParenthesis;
use Calculator\Symbols\OpenParenthesis;
use Calculator\Symbols\SymbolSplitter;

class Calculator
{
    /**
     * @param $line
     * @return int|mixed|string
     */
    public function calc($line)
    {

        try {
            return $this->getLineResult($line);
        } catch(CalculateException $ex) {
            return $ex->getMessage();
        } catch(ValidateException $ex) {
            return $ex->getMessage();
        } catch(\Exception $ex) {
            return $ex->getMessage();
        }
    }

    /**
     * @param $line
     * @return int|mixed
     */
    protected function getLineResult($line)
    {
        $varName = '';
        $this->validateInputLine($line);
        $line = strtolower($line);

        if (strpos($line, '=')) {
            list($varName, $expression) = explode('=', $line);
            $line = $expression;
        }

        $splitter = new SymbolSplitter($line);
        $symbolsList = $splitter->split();

        if (empty($symbolsList)) {
            return 0;
        }

        $symbolsList = $this->setUnaryOperatorMark($symbolsList);

        $nodesList = [];
        $openParenthesysCounter = 0;
        foreach ($symbolsList as $index => $symbol) {
            $nodesList[$index] = new SymbolNode($symbol);
            if (is_a($symbol, OpenParenthesis::class)) {
                $openParenthesysCounter ++;
                continue;
            }
            if (is_a($symbol, CloseParenthesis::class)) {
                $openParenthesysCounter --;
            }
        }

        if ($openParenthesysCounter != 0) {
            throw new ValidateException("Error: Bad expression");
        }

        $groupNode = new GroupNode($this->buildTree($nodesList));
        $result = $this->calcGroupNode($groupNode);
        if (!empty($varName)) {
            VarStorage::setVar($varName, $result);
        }
        return $result;
    }

    /**
     * @param array $nodesList
     * @return array
     */
    protected function buildTree(array $nodesList)
    {
        $tree = [];
        $subTree = []; 
        $openParenthesisCounter = 0;

        foreach ($nodesList as $index => $node) {

            $symbol = $node->getSymbol();
            if (is_a($symbol, OpenParenthesis::class)) {
                $openParenthesisCounter++;

                if ($openParenthesisCounter > 1) {
                    $subTree[] = $node;
                }
            } elseif (is_a($symbol, CloseParenthesis::class)) {
                $openParenthesisCounter--;

                if ($openParenthesisCounter == 0) {
                    $subTree = $this->buildTree($subTree);

                    $tree[] = new GroupNode($subTree);
                    $subTree = [];
                } else {
                    $subTree[] = $node;
                }
            } else {
                if ($openParenthesisCounter == 0) {
                    $tree[] = $node;
                } else {
                    $subTree[] = $node;
                }
            }
        }

        return $tree;
    }

    /**
     * @param AbstractNode $node
     * @return mixed
     */
    protected function calcNode(AbstractNode $node)
    {
        if (is_a($node, SymbolNode::class)) {
            /** @var SymbolNode $node */
            return $this->calcSymbolNode($node);
        } elseif (is_a($node, GroupNode::class)) {
            /** @var GroupNode $node */
            return $this->calcGroupNode($node);
        } else {
            throw new \InvalidArgumentException('Error: Unknown node type "' . get_class($node) . '"');
        }
    }

    /**
     * @param SymbolNode $node
     * @return mixed
     */
    protected function calcSymbolNode(SymbolNode $node)
    {
        $symbol = $node->getSymbol();
        return $symbol->getValue();
    }

    /**
     * @param GroupNode $groupNode
     * @return mixed
     */
    protected function calcGroupNode(GroupNode $groupNode)
    {
        $nodeList = $groupNode->getSubNodes();
        $operatorsList = $this->getOperatorNodeList($nodeList);
        $leftOperand = null;
        $rightOperand = null;


        foreach ($operatorsList as $index => $operatorNode) {
            reset($nodeList);

            while (key($nodeList) < $index) {
                $leftOperand = current($nodeList);
                $leftOperandIndex = key($nodeList);
                next($nodeList);
            }

            $rightOperand = next($nodeList);
            $rightOperandIndex = key($nodeList);
            $rightValue = is_numeric($rightOperand) ? $rightOperand : $this->calcNode($rightOperand);

            $operatorSymbol = $operatorNode->getSymbol();
            if ($operatorSymbol->getIsUnary()) {
                $result = $operatorSymbol->operate(null, $rightValue);
                unset($nodeList[$rightOperandIndex]);
                $nodeList[$index] = $result;
            } else {
                $leftValue = is_numeric($leftOperand) ? $leftOperand : $this->calcNode($leftOperand);

                $result = $operatorSymbol->operate($leftValue, $rightValue);
                unset($nodeList[$leftOperandIndex]);
                unset($nodeList[$rightOperandIndex]);
                $nodeList[$index] = $result;
                $leftOperand = null;
            }
        }
        $result = end($nodeList);

        if (! is_numeric($result) && !is_a($result, AbstractNode::class)) {
            throw new ValidateException("Error: Bad expression");
        }

        if (! is_numeric($result)) {
            return $this->calcNode($result);
        }
        return $result;
    }

    /**
     * @param $line
     */
    private function validateInputLine($line)
    {
        if ($line === null) {
            throw new \InvalidArgumentException('Error: Input must not be null');
        }
        if (! is_string($line)) {
            throw new \InvalidArgumentException(
                'Error: Input must be string but is "' . gettype($line) . '"'
            );
        }
        if ($line === '') {
            throw new \InvalidArgumentException('Error: Input must not be empty');
        }
        //-- for str_split and other functions
        if ((mb_strlen($line) != strlen($line))) {
            throw new \InvalidArgumentException('Error: Input must not contain multibyte characters');
        }
    }

    /**
     * @param array $symbolsList
     * @return array
     */
    private function setUnaryOperatorMark(array $symbolsList)
    {
        $leftOperand = null;
        foreach ($symbolsList as $index => $symbol) {
            if (is_a($symbol, AbstractOperation::class)) {
                if ($leftOperand === null) {
                    ($symbolsList[$index])->markAsUnary();
                    //$symbol->markAsUnary();
                    //print_r($symbol);
                } else {
                    $leftOperand = null;
                }
            } else {
                $leftOperand = $symbol;
            }
        }
        return $symbolsList;
    }

    /**
     * @param array $nodeList
     * @return array
     */
    private function getOperatorNodeList(array $nodeList)
    {
        $opList = [];
        /** @var SymbolNode $node */
        foreach ($nodeList as $index => $node) {
            if (!is_a($node, SymbolNode::class)) {
                continue;
            }
            if (is_a($node->getSymbol(), AbstractOperation::class)) {
                $opList[$index] = $node;
            }
        }

        //-- Returning 1 means $nodeTwo before $nodeOne, returning -1 means $nodeOne before $nodeTwo.
        uasort($opList, function(SymbolNode $nodeOne, SymbolNode $nodeTwo)
        {
            /** @var AbstractOperation $operatorOne */
            $operatorOne = $nodeOne->getSymbol();
            /** @var AbstractOperation $operatorTwo */
            $operatorTwo = $nodeTwo->getSymbol();

            $priorityOne = ($operatorOne->getIsUnary()) ? 20 : 10;
            $priorityTwo = ($operatorTwo->getIsUnary()) ? 20 : 10;

            if ($priorityOne == $priorityTwo) {
                $priorityOne = $operatorOne->getPriority();
                $priorityTwo = $operatorTwo->getPriority();
            }

            //-- if priority eq - set by position
            if ($priorityOne == $priorityTwo) {
                return ($operatorOne->getPosition() < $operatorTwo->getPosition()) ? -1 : 1;
            }
            return ($priorityOne < $priorityTwo) ? 1 : -1;
        });
        
        return $opList;
    }
}