<?php
/**
 */

namespace Calculator;


class VarStorage
{
    private static $varsList = [];

    public static function setVar($varName, $varValue)
    {
        static::$varsList[$varName] = $varValue;
    }

    public static function getVar($varName)
    {
        if (!array_key_exists($varName, static::$varsList)) {
            throw new \InvalidArgumentException('Error: Unknown variable: ' . $varName);
        }
        return static::$varsList[$varName];
    }

    public static function exist($varName)
    {
        return array_key_exists($varName, static::$varsList);
    }

    public static function getNames()
    {
        return array_keys(static::$varsList);
    }
}