<?php
/**
 */

namespace Calculator\Nodes;


class GroupNode extends AbstractNode
{
    protected $subNodesList = [];

    public function __construct(array $symbolsList)
    {
        $this->subNodesList = $symbolsList;
    }

    public function getSubNodes()
    {
        return $this->subNodesList;
    }
}