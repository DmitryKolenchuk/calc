<?php
/**
 */

namespace Calculator\Nodes;


use Calculator\Symbols\AbstractSymbol;

class SymbolNode extends AbstractNode
{
    /**
     * @var AbstractSymbol
     */
    protected $symbol;

    public function __construct(AbstractSymbol $symbol)
    {
        $this->symbol = $symbol;
    }

    /**
     * @return AbstractSymbol
     *
     */
    public function getSymbol()
    {
        return $this->symbol;
    }
}