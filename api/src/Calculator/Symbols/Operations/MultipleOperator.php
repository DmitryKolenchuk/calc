<?php
/**
 */

namespace Calculator\Symbols\Operations;


use Calculator\Symbols\AbstractOperation;

class MultipleOperator extends AbstractOperation
{

    protected $priority = 200;

    public function operate($leftOperand, $rightOperand)
    {
        return $leftOperand * $rightOperand;
    }
}