<?php
/**
 */

namespace Calculator\Symbols\Operations;


use Calculator\Symbols\AbstractOperation;

class SubOperator extends AbstractOperation
{

    protected $priority = 100;

    public function operate($leftOperand, $rightOperand)
    {
        return $leftOperand - $rightOperand;
    }
}