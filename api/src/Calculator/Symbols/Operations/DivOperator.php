<?php
/**
 */

namespace Calculator\Symbols\Operations;


use Calculator\Exceptions\CalculateException;
use Calculator\Symbols\AbstractOperation;

class DivOperator extends AbstractOperation
{

    protected $priority = 200;

    public function operate($leftOperand, $rightOperand)
    {
        if (empty($rightOperand)) {
            throw new CalculateException('Error: Division by zero');
        }
        return $leftOperand / $rightOperand;
    }
}