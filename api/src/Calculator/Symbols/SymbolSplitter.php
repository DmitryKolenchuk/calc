<?php
/**
 */

namespace Calculator\Symbols;


use Calculator\Exceptions\ValidateException;
use Calculator\VarStorage;
use http\Exception\InvalidArgumentException;

class SymbolSplitter
{
    protected $line;

    protected $pos;

    public function __construct($line)
    {
        $this->line = $line;
        $this->pos = 0;
    }

    public function split()
    {
        $symbolList = [];
        $this->pos = 0;
        $index = 0;
        while (($symbol = $this->getSymbol())) {
            $symbolList[$index] = $symbol;
            $index++;
        }
        return $symbolList;
    }

    protected function getSymbol()
    {
        $this->skipSpaces();
        if ($this->pos >= strlen($this->line)) {
            return null;
        }

        $char = $this->getChar();
        $symbolPos = $this->pos;

        if ($this->isLetter($char)) {
            $word = $this->getWord();
            if ($this->isConst($word)) {
                return SymbolFactory::getConstSymbol($word, $symbolPos);
            }
            if (VarStorage::exist($word)) {
                return new NumberSymbol(VarStorage::getVar($word), $symbolPos);
            }
            throw new \InvalidArgumentException('Error: Unknown constant, variable or function: ' . $word);
        }

        if ($this->isNumber($char) || $this->isPeriod($char)) {
            return new NumberSymbol($this->getNumber(), $symbolPos);
        }

        if ($this->isOperator($char)) {
            $this->pos++;
            return SymbolFactory::getOperatorSymbol($char, $symbolPos);
        }

        if ($this->isParenthesis($char)) {
            $this->pos++;
            return SymbolFactory::getParenthesisSymbol($char, $symbolPos);
        }

        if ($char) {
            throw new \InvalidArgumentException('Error: Unrecognised symbol: ' . $char);
        }

        return null;
    }

    protected function skipSpaces()
    {
        while (!is_null($char = $this->getChar()) && $this->isSpace($char)) {
            $this->pos++;
        }
    }

    private function getChar()
    {
        if ($this->pos >= strlen($this->line)) {
            return null;
        }
        return $this->line[$this->pos];
    }

    protected function isSpace($char)
    {
        return in_array($char, [" ", "\t", "\n"]);
    }

    private function isLetter($char)
    {
        if ($char === null) {
            return false;
        }

        $code = ord($char);

        return (($code >= 65 and $code <= 90) or ($code >= 97 and $code <= 122));
    }

    private function getWord()
    {
        $word = '';
        while (!is_null($char = $this->getChar()) && ($this->isLetter($char))) {
            $word .= $char;
            $this->pos++;
        }
        return $word;
    }

    private function isConst($word)
    {
        return in_array($word, ['pi', 'e']);
    }

    private function isNumber($char)
    {
        if (is_null($char)) {
            return false;
        }
        $code = ord($char);
        return ($code >= 48 && $code <= 57);
    }

    private function isPeriod($char)
    {
        return ($char === '.');
    }

    private function isVar($word)
    {
    }

    private function getNumber()
    {
        $number = '';
        while (!is_null($char = $this->getChar()) && ($this->isNumber($char) || $this->isPeriod($char))) {
            $number .= $char;
            $this->pos++;
        }
        if (substr_count($number, '.') > 1) {
            throw new ValidateException("Error: Bad expression");
        }
        return $number;
    }

    private function isOperator($char)
    {
        return in_array($char, ['+', '-', '*', '/']);
    }

    private function isParenthesis($char)
    {
        return in_array($char, ['(', ')']);
    }
}