<?php
/**
 */

namespace Calculator\Symbols;

abstract class AbstractSymbol
{
    protected $value;

    protected $position;

    public function __construct($value, $position)
    {
        $this->value = $value;
        $this->position = $position;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getPosition()
    {
        return $this->position;
    }

}