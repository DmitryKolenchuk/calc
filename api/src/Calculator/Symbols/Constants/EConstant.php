<?php
/**
 */

namespace Calculator\Symbols\Constants;


use Calculator\Symbols\AbstractConstant;

class EConstant extends AbstractConstant
{
    public function getValue()
    {
        return M_E;
    }
}