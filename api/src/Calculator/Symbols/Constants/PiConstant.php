<?php
/**
 */

namespace Calculator\Symbols\Constants;


use Calculator\Symbols\AbstractConstant;

class PiConstant extends AbstractConstant
{
    public function getValue()
    {
        return M_PI;
    }
}