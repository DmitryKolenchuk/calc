<?php
/**
 */

namespace Calculator\Symbols;


use Calculator\Symbols\Constants\EConstant;
use Calculator\Symbols\Constants\PiConstant;
use Calculator\Symbols\Operations\DivOperator;
use Calculator\Symbols\Operations\MultipleOperator;
use Calculator\Symbols\Operations\SubOperator;
use Calculator\Symbols\Operations\SumOperator;

class SymbolFactory
{
    public static function getOperatorSymbol($value, $pos)
    {
        switch ($value) {
            case '+':
                return new SumOperator($value, $pos);
                break;
            case '-':
                return new SubOperator($value, $pos);
                break;
            case '*':
                return new MultipleOperator($value, $pos);
                break;
            case '/':
                return new DivOperator($value, $pos);
                break;
            default:
                throw new \Exception('Error: Unknown operator ' . $value);
        }
    }

    public static function getParenthesisSymbol($value, $pos)
    {
        switch ($value) {
            case '(':
                return new OpenParenthesis($value, $pos);
                break;
            case ')':
                return new CloseParenthesis($value, $pos);
                break;
            default:
                throw new \Exception('Error: Unknown parenthesis ' . $value);
        }
    }

    public static function getConstSymbol($value, $pos)
    {
        switch ($value) {
            case 'pi':
                return new PiConstant($value, $pos);
                break;
            case 'e':
                return new EConstant($value, $pos);
                break;
            default:
                throw new \Exception('Error: Unknown constant ' . $value);
        }
    }
}