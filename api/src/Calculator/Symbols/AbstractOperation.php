<?php
/**
 */

namespace Calculator\Symbols;


abstract class AbstractOperation extends AbstractSymbol
{
    protected $priority = 0;

    protected $isUnary = false;

    abstract public function operate($leftOperand, $rightOperand);

    public function getPriority()
    {
        return $this->priority;
    }

    public function getIsUnary()
    {
        return $this->isUnary;
    }

    public function markAsUnary()
    {
        $this->isUnary = true;
    }
}