PHP application for calculating expressions in strings

For work with console application try example
&lt;?php

require_once \_\_DIR\_\_ . '/../vendor/autoload.php';

$calc = new \Calculator\Calculator();

$lines = [

    'var=5+6',
    
    '5+var',
    
    '3/0',
];


foreach ($lines as $line) {

    echo $calc->calc($line) . "\n";

}